//
//  ManagerAbsract.cpp
//  streamjoins
//
//  Created by Hassan Najeeb on 06/06/16.
//  Copyright © 2016 Hassan Najeeb. All rights reserved.
//

#include "ManagerAbsract.hpp"

ManagerAbstract::ManagerAbstract(){
    
}
ManagerAbstract::~ManagerAbstract(){
    // delete this;
}

void ManagerAbstract::receiveTupleStore(Standardtuple *tuple, RangeTable* rt, Standardtuple** w, int *count, std::queue<int> *entrySequence, Standardtuple** extendedWindow){
    
    int partition = 0;
    
    int numOfPartitions = rt->getRSize();
    
    partition = PartitionFinder::getPartition(numOfPartitions, tuple->key, rt);
    
    int lenOfPartition = rt->getSubWindowSize();
    int head = rt->table[partition][2];
    int tail = rt->table[partition][3];
    int elenght = rt->getThreshold()*multipleOf;
    int ehead = rt->table[partition][11];
    int etail = rt->table[partition][12];
    
    /*when no extended memory is being used for this partition*/
    if(rt->table[partition][10] == -1){
        
        //normal condition so dont have to go through so many checks
        if(head>=tail && head < lenOfPartition-1){
            insertInHead(entrySequence, w, partition, head, rt, tuple);
        }
        else if(head>=tail && head>=lenOfPartition-1){
            if(tail>0){
                head = -1;
                rt->table[partition][2] = head;
                insertInHead(entrySequence, w, partition, head, rt, tuple);
            }
            else if (tail == 0){
#ifdef whichPartitionExtended
                std::cout << "Partition extended " << partition << tuple->belongsTo <<"\n";
#endif
                extendedWindow[partition] = new Standardtuple[rt->getThreshold()*multipleOf](); //create new extended memory
                rt->table[partition][10] = 1; //turn the extended memory bit on for this partition
                rt->table[partition][13] = head; //mark the jump pointer
                int exPLen = rt->getThreshold()*multipleOf;
                for(int j=0; j<exPLen;j++){
                    extendedWindow[partition][j] = {};
                }
                
            }
        }
        //when head is less than tail and head wont overwrite the tail then
        else if (head < tail){
            if(head+1 != tail){
                insertInHead(entrySequence, w, partition, head, rt, tuple);
            }
            else if (head+1 == tail){
#ifdef whichPartitionExtended
                std::cout << "Partition extended " << partition << tuple->belongsTo <<"\n";
#endif
                extendedWindow[partition] = new Standardtuple[rt->getThreshold()*multipleOf]; //create new extended memory
                rt->table[partition][10] = 1; //turn the extended memory bit on for this partition
                rt->table[partition][13] = head; //mark the jump pointer
                int exPLen = rt->getThreshold()*multipleOf;
                for(int j=0; j<exPLen;j++){
                    extendedWindow[partition][j] = {};
                }
            }
        }
        else{
            std::cout <<"something else is wrong";
        }
    }
    
    //when extended memory bit is on
    if(rt->table[partition][10] == 1){
        
        if(ehead >= etail && ehead < elenght-1){
            insertInEHead(entrySequence, extendedWindow, partition, ehead, rt, tuple);
        }
        else if (ehead >= etail && ehead>=elenght-1){
            if(head >= tail && head<lenOfPartition-1){
                insertInHead(entrySequence, w, partition, head, rt, tuple);
            }
            else if(head >= tail && head >= lenOfPartition-1 && tail>0){
                head = -1;
                rt->table[partition][2] = head;
                insertInHead(entrySequence, w, partition, head, rt, tuple);
            }
            else if (head < tail && head+1 != tail){
                insertInHead(entrySequence, w, partition, head, rt, tuple);
            }
            else if (etail > 0){
                ehead = -1;
                rt->table[partition][11] = ehead;
                insertInEHead(entrySequence, extendedWindow, partition, ehead, rt, tuple);
            }
            else{
                *count= *count-1;
                std::cout << "\n we're full I \n";
                for(int i =0 ; i< rt->getCSize(); i++){
                    std::cout << rt->table[partition-1][i] << ",";
                }
                std::cout << "\n";
                
                for(int i =0 ; i< rt->getCSize(); i++){
                    std::cout << rt->table[partition][i] << ",";
                }
                std::cout << "\n";
                
                for(int i =0 ; i< rt->getCSize(); i++){
                    std::cout << rt->table[partition+1][i] << ",";
                }
                std::cout << "\n";
            }
        }
        else if (ehead < etail && ehead+1 != etail){
            insertInEHead(entrySequence, extendedWindow, partition, ehead, rt, tuple);
        }
        else if (ehead < etail && ehead+1 == etail){
            if(head >= tail && head<lenOfPartition-1){
                insertInHead(entrySequence, w, partition, head, rt, tuple);
            }
            else if(head >= lenOfPartition-1 && tail>0){
                head = -1;
                rt->table[partition][2] = head;
                insertInHead(entrySequence, w, partition, head, rt, tuple);
            }
            else if (head < tail && head+1 != tail){
                insertInHead(entrySequence, w, partition, head, rt, tuple);
            }
            else{
                *count= *count-1;
                std::cout << "\n we're full II \n";
                for(int i =0 ; i< rt->getCSize(); i++){
                    std::cout << rt->table[partition-1][i] << ",";
                }
                std::cout << "\n";
                
                for(int i =0 ; i< rt->getCSize(); i++){
                    std::cout << rt->table[partition][i] << ",";
                }
                std::cout << "\n";
                
                for(int i =0 ; i< rt->getCSize(); i++){
                    std::cout << rt->table[partition+1][i] << ",";
                }
                std::cout << "\n";
            }
        }
        else{
            std::cout << "come here yo?";
        }
        
    }
    
    
    //when total count of window is getting bigger than window size then need to expire some tuples
    if (*count>=rt->getWindowSize()) {
        expirator(entrySequence, rt, w, count, extendedWindow);
    }
    //when count gets bigger than threshold and boundaryadjuster bit is off i.e. 0 then call the boundary adjuster
    if (rt->table[partition][9]>rt->table[partition][8] && boundaryAdjusted == false) {
        
#ifdef boundaryAdjusterDivideBy2
        boundaryAdjusterHalf(w[partition], rt, partition);
#endif
#ifdef boundaryAdjusterDivideby4
        boundaryAdjuster(w[partition], rt, partition);
#endif
        
        
#ifdef printWholeRangeTableAfterBA
        std::cout << "\nRange Table After Adjustment\n";
        for(int k=0;k<rt->getRSize();k++){
            for(int l=0;l<rt->getCSize();l++){
                std::cout << rt->table[k][l] << ",";
            }
            std::cout << "\n";
        }
        std::cout << "\n";
#endif
#ifdef printOnlyAdjustedPartitons
        int columns = rt->getCSize();
        int adjPar = rt->getAdjustedPartition();
        int adjParNei = rt->getAdjustedPartitionNeighbor();
        for(int k=0;k<columns;k++){
            std::cout << rt->table[adjPar][k] << ",";
        }
        std::cout << "\n";
        for(int k=0;k<columns;k++){
            std::cout << rt->table[adjParNei][k] << ",";
        }
        std::cout << "\n";
#endif
    }
}

void ManagerAbstract::expirator(std::queue<int> *entrySequence, RangeTable* rt,Standardtuple** w, int *count, Standardtuple** extendedWindow){
    
    //int numOfPartitions = rt->getRSize();
    int deleteFromPartition;
    int tail;
    int head;
    int ehead;
    int etail;
    int jumpPointer;
    int lenOfPartition = rt->getSubWindowSize();
    int elength = rt->getThreshold()*multipleOf;
    
    //for(int i = 0; i<numOfPartitions; i++){
    deleteFromPartition = entrySequence->front();
    tail =  rt->table[deleteFromPartition][3];
    head =  rt->table[deleteFromPartition][2];
    
    if(rt->table[deleteFromPartition][10] == -1){
        //std::cout<< rt->table[deleteFromPartition][3];
        
        //if tail is at the end of array then tail should goto to the start of the array
        if (rt->table[deleteFromPartition][3] >= lenOfPartition-1) {
            
            rt->table[deleteFromPartition][3] = -1;
            tail = -1;
            
        }
        tail = tail+1;
        w[deleteFromPartition][tail]= {};
        
        deleteFromTail(rt,deleteFromPartition,entrySequence,count);
        
    }
    
    //when extended memory bit is on
    if(rt->table[deleteFromPartition][10] == 1){
        jumpPointer = rt->table[deleteFromPartition][13];
        etail = rt->table[deleteFromPartition][12];
        ehead =  rt->table[deleteFromPartition][11];
        
        if(tail != jumpPointer){
            if(tail != head){
                if(tail >= lenOfPartition-1){
                    rt->table[deleteFromPartition][3] = -1;
                    tail = -1;
                }
                tail = tail+1;
                w[deleteFromPartition][tail]= {};
                deleteFromTail(rt,deleteFromPartition,entrySequence,count);
            }
            else if(tail == head){
                if(etail != ehead){
                    if(etail >= elength-1){
                        rt->table[deleteFromPartition][12] = -1;
                        etail = -1;
                    }
                    etail = etail+1;
                    extendedWindow[deleteFromPartition][etail]= {};
                    deleteFromETail(rt,deleteFromPartition,entrySequence,count);
                }
                else{
                    std::cout << "Nothing to delete I";
                }
                // TODO delete from ehead
            }
        }
        else if(tail == jumpPointer){
            if(etail < elength-1){
                if(etail != ehead){
                    etail = etail+1;
                    extendedWindow[deleteFromPartition][etail]= {};
                    deleteFromETail(rt,deleteFromPartition,entrySequence,count);
                }
                else if(etail == ehead){
                    if(tail != head){
                        if(tail >= lenOfPartition-1){
                            rt->table[deleteFromPartition][3] = -1;
                            tail = -1;
                        }
                        tail = tail+1;
                        w[deleteFromPartition][tail]= {};
                        deleteFromTail(rt,deleteFromPartition,entrySequence,count);
                    }
                    else{
                        std::cout << "Nothing to delete II\n";
                        std::cout <<"\n";
                    }
                }
            }
            else if (etail>=elength-1){
                if(tail != head){
                    if(tail >= lenOfPartition-1){
                        rt->table[deleteFromPartition][3] = -1;
                        tail = -1;
                    }
                    tail = tail+1;
                    w[deleteFromPartition][tail]= {};
                    deleteFromTail(rt,deleteFromPartition,entrySequence,count);
                    
                }
                else if (tail == head){
                    if(etail != ehead){
                        etail = -1;
                        rt->table[deleteFromPartition][12] = etail;
                        etail = etail+1;
                        extendedWindow[deleteFromPartition][etail]= {};
                        deleteFromETail(rt,deleteFromPartition,entrySequence,count);
                    }
                    else{
                        std::cout << "Nothing to delete III";
                    }
                }
            }
        }
        else{
            std::cout << "tell me something is wrong";
        }
        
    } //if extended memory bit is on
    
    //To check if boundary adjustment is completed
    if (boundaryAdjusted && deleteFromPartition == rt->getAdjustedPartition()) {
        checkBoundaryAdjustmentCompletetion(rt);
    }//To check if boundary adjustment is completed
    
    
    //when etail and ehead are equal to elength
    if(rt->table[deleteFromPartition][10] == 1 && rt->table[deleteFromPartition][12] == rt->table[deleteFromPartition][11]){
        rt->table[deleteFromPartition][10] = -1; //turn the extended memory bit off
        rt->table[deleteFromPartition][13] = -1;
        rt->table[deleteFromPartition][11] = 0;
        rt->table[deleteFromPartition][12] = 0;
        rt->table[deleteFromPartition][14] = -1;
        rt->table[deleteFromPartition][15] = -1;
        delete extendedWindow[deleteFromPartition];
        extendedWindow[deleteFromPartition] = nullptr;
#ifdef whichExtendedPartitionDeleted
        std::cout << "to delete extended partiton " << deleteFromPartition << "\n";
#endif
    }
}

void ManagerAbstract::receiveTupleProbe(Standardtuple *tuple, RangeTable* rt, Standardtuple** w, Standardtuple** extendedWindow, Joiner * joiner){
    //TODO add functionality for extended memory
    int numOfPartitions = rt->getRSize();
    
#ifndef testBit
    int i = PartitionFinder::getPartition(numOfPartitions, tuple->key, rt);
    
    //        for (int i=0; i<numOfPartitions; i++) {
    if (rt->table[i][0] <= tuple->key && rt->table[i][1] >= tuple->key) {
        //send the (i)tuple, (ii)probe window, (iii)head, (iv)tail, ,(v)length of partition
        joiner->receiveTuple(tuple, w[i], rt->table[i][2], rt->table[i][3], rt->getSubWindowSize(), extendedWindow, rt->table[i][10], rt->table[i][11], rt->table[i][12], rt->table[i][8]*multipleOf, i, rt);
        
    }
    //when boundary is adjusted and oldmin/oldmax is not null
    if(boundaryAdjusted == true){
        int adjPartition = rt->getAdjustedPartition();
        i = adjPartition;
        //if oldMin is not Initial and NOT oldmin lies between min and max and if key lies between oldmin
        if(rt->table[i][4]!=-1 && (rt->table[i][4] <= tuple->key &&  tuple->key<= (rt->table[i][0]-1))){
            joiner->receiveTuple(tuple, w[i], rt->table[i][6], rt->table[i][3], rt->getSubWindowSize(), extendedWindow, rt->table[i][10], rt->table[i][14], rt->table[i][12], rt->table[i][8]*multipleOf, i, rt);
        }//if oldmin is not null
        
        //if oldMax is not Initial  and NOT oldMax lies between min and max and if key lies between Max and oldMax
        if(rt->table[i][5]!=-1 && ((rt->table[i][1]+1) <= tuple->key &&  tuple->key <= rt->table[i][5])){
            joiner->receiveTuple(tuple, w[i], rt->table[i][6], rt->table[i][3], rt->getSubWindowSize(), extendedWindow, rt->table[i][10], rt->table[i][14], rt->table[i][12], rt->table[i][8]*multipleOf, i, rt);
        }// if max is not null
    }//boundary adjustment condition
    //        }// for loop
#endif
    
#ifdef testBit
    joiner->testJoin(tuple, w, numOfPartitions, rt->getSubWindowSize(),rt, extendedWindow);
#endif
}

#ifdef boundaryAdjusterDivideby4
void ManagerAbstract::boundaryAdjuster(Standardtuple* window, RangeTable* rt, int partition){
    
    //TODO add the old head and old tail as well
    
    int leftPartition = partition - 1;
    int rightPartition = partition + 1;
    
    if(partition == 0){
        if(rt->table[partition][9] > rt->table[rightPartition][9]){
            int newBoundary = 0;
            newBoundary = rt->table[rightPartition][0] - (rt->table[partition][1]-rt->table[partition][0]+1)/4;
            if(newBoundary-1 >= rt->table[partition][0]){
                rt->table[rightPartition][4] =  rt->table[rightPartition][0];//old min right partition
                rt->table[partition][5] = rt->table[partition][1];//old max partition
                
                rt->table[rightPartition][0] = newBoundary;
                rt->table[partition][1] = rt->table[rightPartition][0]-1;
                rt->table[partition][6] = rt->table[partition][2]; //old head partition
                rt->table[partition][7] = rt->table[partition][3]; //old tail partition
                
                rt->table[rightPartition][6] = rt->table[rightPartition][2]; //old head right partition
                rt->table[rightPartition][7] = rt->table[rightPartition][3]; //old tail right partition
                
                if(rt->table[partition][10] == 1){
                    rt->table[partition][14] = rt->table[partition][11]; //old ehead left partition
                    rt->table[partition][15] = rt->table[partition][12]; //old etail left partition
                }
                if(rt->table[rightPartition][10] == 1){
                    rt->table[rightPartition][14] = rt->table[rightPartition][11]; //old ehead left partition
                    rt->table[rightPartition][15] = rt->table[rightPartition][12]; //old etail left partition
                }
                
                rt->setAdjustedPartition(partition);
                rt->setAdjustedPartitionNeighbor(rightPartition);
                boundaryAdjusted = true; //bit turned on
            }
        }
    }
    else if(partition == rt->getRSize()-1){
        if(rt->table[partition][9] > rt->table[leftPartition][9]){
            int newBoundary = 0;
            newBoundary = rt->table[leftPartition][1] + (rt->table[partition][1]-rt->table[partition][0]+1)/4;
            if(newBoundary+1 <= rt->table[partition][1]){
                rt->table[leftPartition][5] = rt->table[leftPartition][1]; //old max left partition
                rt->table[partition][4] = rt->table[partition][0];//old min partition
                
                rt->table[leftPartition][1] = newBoundary;
                rt->table[partition][0] = rt->table[leftPartition][1]+1;//new min partition
                
                rt->table[partition][6] = rt->table[partition][2]; //old head partition
                rt->table[partition][7] = rt->table[partition][3]; //old tail partition
                
                rt->table[leftPartition][6] = rt->table[leftPartition][2]; //old head left partition
                rt->table[leftPartition][7] = rt->table[leftPartition][3]; //old tail left partition
                
                if(rt->table[partition][10] == 1){
                    rt->table[partition][14] = rt->table[partition][11]; //old ehead left partition
                    rt->table[partition][15] = rt->table[partition][12]; //old etail left partition
                }
                if(rt->table[leftPartition][10] == 1){
                    rt->table[leftPartition][14] = rt->table[leftPartition][11]; //old ehead left partition
                    rt->table[leftPartition][15] = rt->table[leftPartition][12]; //old etail left partition
                }
                
                rt->setAdjustedPartition(partition);
                rt->setAdjustedPartitionNeighbor(leftPartition);
                boundaryAdjusted = true; //bit turned on
            }
        }
    }
    else if(rt->table[leftPartition][9] < rt->table[rightPartition][9]){
        if(rt->table[partition][9] > rt->table[leftPartition][9]){
            int newBoundary = 0;
            newBoundary = rt->table[leftPartition][1] + (rt->table[partition][1]-rt->table[partition][0]+1)/4;
            if(newBoundary+1 <= rt->table[partition][1]){
                rt->table[leftPartition][5] = rt->table[leftPartition][1]; //old max left partition
                rt->table[partition][4] = rt->table[partition][0];//old min partition
                
                rt->table[leftPartition][1] = newBoundary;
                rt->table[partition][0] = rt->table[leftPartition][1]+1;//new min partition
                
                rt->table[partition][6] = rt->table[partition][2]; //old head partition
                rt->table[partition][7] = rt->table[partition][3]; //old tail partition
                
                rt->table[leftPartition][6] = rt->table[leftPartition][2]; //old head left partition
                rt->table[leftPartition][7] = rt->table[leftPartition][3]; //old tail left partition
                
                if(rt->table[partition][10] == 1){
                    rt->table[partition][14] = rt->table[partition][11]; //old ehead left partition
                    rt->table[partition][15] = rt->table[partition][12]; //old etail left partition
                }
                if(rt->table[leftPartition][10] == 1){
                    rt->table[leftPartition][14] = rt->table[leftPartition][11]; //old ehead left partition
                    rt->table[leftPartition][15] = rt->table[leftPartition][12]; //old etail left partition
                }
                
                rt->setAdjustedPartition(partition);
                rt->setAdjustedPartitionNeighbor(leftPartition);
                
                boundaryAdjusted = true; //bit turned on
            }
        }
    }
    else if(rt->table[leftPartition][9] > rt->table[rightPartition][9]){
        if(rt->table[partition][9] > rt->table[rightPartition][9]){
            int newBoundary = 0;
            newBoundary = rt->table[rightPartition][0] - (rt->table[partition][1]-rt->table[partition][0]+1)/4;
            if(newBoundary-1 >= rt->table[partition][0]){
                
                rt->table[rightPartition][4] =  rt->table[rightPartition][0];//old min right partition
                rt->table[partition][5] = rt->table[partition][1];//old max partition
                
                rt->table[rightPartition][0] = newBoundary;
                rt->table[partition][1] = rt->table[rightPartition][0]-1;
                
                rt->table[partition][6] = rt->table[partition][2]; //old head partition
                rt->table[partition][7] = rt->table[partition][3]; //old tail partition
                
                if(rt->table[partition][10] == 1){
                    rt->table[partition][14] = rt->table[partition][11]; //old ehead left partition
                    rt->table[partition][15] = rt->table[partition][12]; //old etail left partition
                }
                if(rt->table[rightPartition][10] == 1){
                    rt->table[rightPartition][14] = rt->table[rightPartition][11]; //old ehead left partition
                    rt->table[rightPartition][15] = rt->table[rightPartition][12]; //old etail left partition
                }
                
                rt->table[rightPartition][6] = rt->table[rightPartition][2]; //old head right partition
                rt->table[rightPartition][7] = rt->table[rightPartition][3]; //old tail right partition
                
                rt->setAdjustedPartition(partition);
                rt->setAdjustedPartitionNeighbor(rightPartition);
                
                boundaryAdjusted = true; //bit turned on
            }
        }
    }
#ifdef whichPatitionsAdjusted
    std::cout << "Boundary Ajusted For " << rt->getAdjustedPartition() << " and " << rt->getAdjustedPartitionNeighbor()<< "\n";
#endif
}
#endif //#ifdef boundaryAdjusterDivideby4


#ifdef boundaryAdjusterDivideBy2
void ManagerAbstract::boundaryAdjusterHalf(Standardtuple* window, RangeTable* rt, int partition){
    //
    //    lastBoundaryAdjusted = std::chrono::high_resolution_clock::now();
    //
    //    auto duration = std::chrono::duration<std::chrono::seconds>(lastBoundaryAdjusted-(std::chrono::high_resolution_clock::now()-std::chrono::high_resolution_clock::now()));
    //    std::cout << "Last adjustment call was: "<<duration;
    int a=0;
    int leftPartition = partition - 1;
    int rightPartition = partition + 1;
    int adjPartition = rt->getAdjustedPartition();
    int adjPartitionNeigbor = rt->getAdjustedPartitionNeighbor();
    
    if(partition == 0){
        if(rt->table[partition][9] > rt->table[rightPartition][9]){
            
            int newBoundary = 0;
            if(rightPartition == adjPartition && partition == adjPartitionNeigbor){
                //old max and max divided by 2
                
                newBoundary = (rt->table[partition][5]+rt->table[partition][1])/2;
                
            }
            else{
                newBoundary = (rt->table[partition][0]+rt->table[partition][1])/2;
            }
            
            if(newBoundary-1 >= rt->table[partition][0]){
                rt->table[rightPartition][4] =  rt->table[rightPartition][0];//old min right partition
                rt->table[partition][5] = rt->table[partition][1];//old max partition
                
                rt->table[rightPartition][0] = newBoundary;
                rt->table[partition][1] = rt->table[rightPartition][0]-1;
                rt->table[partition][6] = rt->table[partition][2]; //old head partition
                rt->table[partition][7] = rt->table[partition][3]; //old tail partition
                
                rt->table[rightPartition][6] = rt->table[rightPartition][2]; //old head right partition
                rt->table[rightPartition][7] = rt->table[rightPartition][3]; //old tail right partition
                
                if(rt->table[partition][10] == 1){
                    rt->table[partition][14] = rt->table[partition][11]; //old ehead left partition
                    rt->table[partition][15] = rt->table[partition][12]; //old etail left partition
                }
                if(rt->table[rightPartition][10] == 1){
                    rt->table[rightPartition][14] = rt->table[rightPartition][11]; //old ehead left partition
                    rt->table[rightPartition][15] = rt->table[rightPartition][12]; //old etail left partition
                }
                a=1;
                rt->setAdjustedPartition(partition);
                rt->setAdjustedPartitionNeighbor(rightPartition);
                boundaryAdjusted = true; //bit turned on
            }
        }
    }
    else if(partition == rt->getRSize()-1){
        if(rt->table[partition][9] > rt->table[leftPartition][9]){
            int newBoundary = 0;
            if(leftPartition == adjPartition && partition == adjPartitionNeigbor){
                //min and oldmin divided by 2
                newBoundary = (rt->table[partition][0]+rt->table[partition][4])/2;//new max left partition;
                
            }
            else{
                newBoundary = (rt->table[partition][0]+rt->table[partition][1])/2;//new max left partition
            }
            if(newBoundary+1 <= rt->table[partition][1]){
                
                rt->table[leftPartition][5] = rt->table[leftPartition][1]; //old max left partition
                rt->table[partition][4] = rt->table[partition][0];//old min partition
                
                rt->table[leftPartition][1] = newBoundary;
                rt->table[partition][0] = rt->table[leftPartition][1]+1;//new min partition
                
                rt->table[partition][6] = rt->table[partition][2]; //old head partition
                rt->table[partition][7] = rt->table[partition][3]; //old tail partition
                
                rt->table[leftPartition][6] = rt->table[leftPartition][2]; //old head left partition
                rt->table[leftPartition][7] = rt->table[leftPartition][3]; //old tail left partition
                
                if(rt->table[partition][10] == 1){
                    rt->table[partition][14] = rt->table[partition][11]; //old ehead left partition
                    rt->table[partition][15] = rt->table[partition][12]; //old etail left partition
                }
                if(rt->table[leftPartition][10] == 1){
                    rt->table[leftPartition][14] = rt->table[leftPartition][11]; //old ehead left partition
                    rt->table[leftPartition][15] = rt->table[leftPartition][12]; //old etail left partition
                }
                a=2;
                rt->setAdjustedPartition(partition);
                rt->setAdjustedPartitionNeighbor(leftPartition);
                boundaryAdjusted = true; //bit turned on
            }
        }
    }
    else if(rt->table[leftPartition][9] < rt->table[rightPartition][9]){
        if(rt->table[partition][9] > rt->table[leftPartition][9]){
            
            int newBoundary = 0;
            if(leftPartition == adjPartition && partition == adjPartitionNeigbor){
                //min and oldmin divided by 2
                newBoundary = (rt->table[partition][0]+rt->table[partition][4])/2;//new max left partition;
                
            }
            else{
                newBoundary = (rt->table[partition][0]+rt->table[partition][1])/2;//new max left partition
            }
            if(newBoundary+1 <= rt->table[partition][1]){
                rt->table[leftPartition][5] = rt->table[leftPartition][1]; //old max left partition
                rt->table[partition][4] = rt->table[partition][0];//old min partition
                
                rt->table[leftPartition][1] = newBoundary;
                rt->table[partition][0] = rt->table[leftPartition][1]+1;//new min partition
                
                rt->table[partition][6] = rt->table[partition][2]; //old head partition
                rt->table[partition][7] = rt->table[partition][3]; //old tail partition
                
                rt->table[leftPartition][6] = rt->table[leftPartition][2]; //old head left partition
                rt->table[leftPartition][7] = rt->table[leftPartition][3]; //old tail left partition
                
                if(rt->table[partition][10] == 1){
                    rt->table[partition][14] = rt->table[partition][11]; //old ehead left partition
                    rt->table[partition][15] = rt->table[partition][12]; //old etail left partition
                }
                if(rt->table[leftPartition][10] == 1){
                    rt->table[leftPartition][14] = rt->table[leftPartition][11]; //old ehead left partition
                    rt->table[leftPartition][15] = rt->table[leftPartition][12]; //old etail left partition
                }
                a=3;
                rt->setAdjustedPartition(partition);
                rt->setAdjustedPartitionNeighbor(leftPartition);
                
                boundaryAdjusted = true; //bit turned on
            }
        }
    }
    else if(rt->table[leftPartition][9] > rt->table[rightPartition][9]){
        if(rt->table[partition][9] > rt->table[rightPartition][9]){
            int newBoundary = 0;
            if(rightPartition == adjPartition && partition == adjPartitionNeigbor){
                //old max and max divided by 2
                
                newBoundary = (rt->table[partition][5]+rt->table[partition][1])/2;
                
            }
            else{
                newBoundary = (rt->table[partition][0]+rt->table[partition][1])/2;
            }
            
            if(newBoundary-1 >= rt->table[partition][0]){
                rt->table[rightPartition][4] =  rt->table[rightPartition][0];//old min right partition
                rt->table[partition][5] = rt->table[partition][1];//old max partition
                
                rt->table[rightPartition][0] = newBoundary;
                rt->table[partition][1] = rt->table[rightPartition][0]-1;
                
                rt->table[partition][6] = rt->table[partition][2]; //old head partition
                rt->table[partition][7] = rt->table[partition][3]; //old tail partition
                
                if(rt->table[partition][10] == 1){
                    rt->table[partition][14] = rt->table[partition][11]; //old ehead left partition
                    rt->table[partition][15] = rt->table[partition][12]; //old etail left partition
                }
                if(rt->table[rightPartition][10] == 1){
                    rt->table[rightPartition][14] = rt->table[rightPartition][11]; //old ehead left partition
                    rt->table[rightPartition][15] = rt->table[rightPartition][12]; //old etail left partition
                }
                a=4;
                rt->table[rightPartition][6] = rt->table[rightPartition][2]; //old head right partition
                rt->table[rightPartition][7] = rt->table[rightPartition][3]; //old tail right partition
                
                rt->setAdjustedPartition(partition);
                rt->setAdjustedPartitionNeighbor(rightPartition);
                
                boundaryAdjusted = true; //bit turned on
            }
        }
    }
    
    if(rt->table[partition][0]>rt->table[partition][1] || rt->table[rt->getAdjustedPartitionNeighbor()][0]>rt->table[rt->getAdjustedPartitionNeighbor()][1]){
        
        std::cout << a <<";"<< rt->table[partition][0] << ";"<< rt->table[partition][1] << ";" <<rt->table[partition][4] << ";"<< rt->table[partition][5] << ";"<< rt->table[rt->getAdjustedPartitionNeighbor()][0] << ";"<< rt->table[rt->getAdjustedPartitionNeighbor()][1];
        std::cout << "\n";
        
    }
#ifdef whichPatitionsAdjusted
    std::cout << "Boundary Ajusted For " << rt->getAdjustedPartition() << " and " << rt->getAdjustedPartitionNeighbor()<< "\n";
#endif
}
#endif //ifdef boundaryadjusterdivideby2


void ManagerAbstract::insertInHead(std::queue<int> *entrySequence,Standardtuple** w,int partition, int head, RangeTable* rt, Standardtuple* tuple){
    entrySequence->push(partition);
    head = head+1;
    w[partition][head]= *tuple;
    rt->table[partition][2]++;  //head pointer has last element and is increased by 1
    rt->table[partition][9]++;  //count increased by 1
}

void ManagerAbstract::insertInEHead(std::queue<int> *entrySequence,Standardtuple** extendedWindow,int partition, int ehead, RangeTable* rt, Standardtuple* tuple){
    entrySequence->push(partition);
    ehead = ehead+1;
    extendedWindow[partition][ehead] = *tuple;
    rt->table[partition][11]++; //ehead incremented by 1
    rt->table[partition][9]++;  //count increased by 1
}

void ManagerAbstract::deleteFromTail(RangeTable* rt, int deleteFromPartition, std::queue<int> *entrySequence, int* count){
    rt->table[deleteFromPartition][3]++; // tail pointer has 1st element and is increased by 1 in case of deletion
    rt->table[deleteFromPartition][9]--; // count decreased by 1 in case of deletion
    entrySequence->pop();
    *count= *count-1; // decrease total count
}

void ManagerAbstract::deleteFromETail(RangeTable* rt, int deleteFromPartition, std::queue<int> *entrySequence, int* count){
    rt->table[deleteFromPartition][12]++; // etail pointer is increased by 1 in case of deletion
    rt->table[deleteFromPartition][9]--; // count decreased by 1 in case of deletion
    entrySequence->pop();
    *count= *count-1; // decrease total count
}

void ManagerAbstract::checkBoundaryAdjustmentCompletetion(RangeTable * rt){
    int partition = rt->getAdjustedPartition();
    
    //when new tail is equal to oldHead
    if(rt->table[partition][3] == rt->table[partition][6]){
        int neighbor = rt->getAdjustedPartitionNeighbor();
        //if extended partition is also on
        if(rt->table[partition][10] == 1 && rt->table[partition][14] != -1 && rt->table[partition][15] != -1){
            //        if(rt->table[partition][14] != -1 && rt->table[partition][15] != -1){
            //when new etail is equal to oldEHead
            if(rt->table[partition][12] == rt->table[partition][14]){
                rt->table[partition][6] = -1; //old head
                rt->table[partition][7] = -1; //old tail
                rt->table[partition][14] = -1; //old ehead
                rt->table[partition][15] = -1; //old etail
                
                rt->table[neighbor][6] = -1; //old head
                rt->table[neighbor][7] = -1; //old tail
                rt->table[neighbor][14] = -1; //old ehead
                rt->table[neighbor][15] = -1; //old etail
                boundaryAdjusted = false;
            }
        }
        else{
            rt->table[partition][6] = -1; //old head
            rt->table[partition][7] = -1; //old tail
            
            rt->table[neighbor][6] = -1; //old head
            rt->table[neighbor][7] = -1; //old tail
            
            boundaryAdjusted = false;
        }
    }
    
}

//void ManagerAbstract::withExtendedBAC(int partition, int neighbor, RangeTable * rt){
//
//    rt->table[partition][4] = -1; //old min
//    rt->table[partition][5] = -1; //old max
//    rt->table[partition][6] = -1; //old head
//    rt->table[partition][7] = -1; //old tail
//    rt->table[partition][14] = -1; //old ehead
//    rt->table[partition][15] = -1; //old etail
//
//    rt->table[neighbor][4] = -1; //old min
//    rt->table[neighbor][5] = -1; //old max
//    rt->table[neighbor][6] = -1; //old head
//    rt->table[neighbor][7] = -1; //old tail
//    rt->table[neighbor][14] = -1; //old ehead
//    rt->table[neighbor][15] = -1; //old etail
//    boundaryAdjusted = false;
//#ifdef whenBAComplete
//    std::cout << "\n" << "Ajusted complete " << rt->getAdjustedPartition() << " and " << rt->getAdjustedPartitionNeighbor()<< "\n";
//#endif
//}
//
//void ManagerAbstract::BAC(int partition, int neighbor, RangeTable * rt){
//
//    rt->table[partition][4] = -1; //old min
//    rt->table[partition][5] = -1; //old max
//    rt->table[partition][6] = -1; //old head
//    rt->table[partition][7] = -1; //old tail
//
//    rt->table[neighbor][4] = -1; //old min
//    rt->table[neighbor][5] = -1; //old max
//    rt->table[neighbor][6] = -1; //old head
//    rt->table[neighbor][7] = -1; //old tail
//
//    boundaryAdjusted = false;
//#ifdef whenBAComplete
//    std::cout << "\n" << "Ajusted complete " << rt->getAdjustedPartition() << " and " << rt->getAdjustedPartitionNeighbor()<< "\n";
//#endif
//}
