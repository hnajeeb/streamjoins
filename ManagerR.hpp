//
//  ManagerR.hpp
//  streamjoins
//
//  Created by Hassan Najeeb on 08/06/16.
//  Copyright © 2016 Hassan Najeeb. All rights reserved.
//

#ifndef ManagerR_hpp
#define ManagerR_hpp
#include "ManagerAbsract.hpp"
#include "RwRangeTable.hpp"
#include "tuple.hpp"
#include <stdio.h>
#include <queue>

class ManagerR: public ManagerAbstract{
    
private:
    int count;
    int *ptrCount;
    std::queue<int> *entrySequence;
    
public:
    RwRangeTable* rrt;
    Standardtuple** rWindow;
    Standardtuple** extendedRWindow;
    Joiner *joiner;
    ManagerR();
    ManagerR(int windowSize, int rSize, int cSize, int minRange, int maxRange);
    void receiveTupleStore(Standardtuple *tuple);
    
    void receiveTupleProbe(Standardtuple *tuple);
    ~ManagerR();
};
#endif /* ManagerR_hpp */
