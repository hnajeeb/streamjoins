//
//  ManagerS.cpp
//  streamjoins
//
//  Created by Hassan Najeeb on 08/06/16.
//  Copyright © 2016 Hassan Najeeb. All rights reserved.
//

#include "ManagerS.hpp"

ManagerS::ManagerS(){
    srt= SwRangeTable::Singleton();
    sWindow = new Standardtuple*[srt->getRSize()];
    extendedSWindow = new Standardtuple*[srt->getRSize()];
    entrySequence = new std::queue<int>[srt->getSubWindowSize()*srt->getRSize()];
    this->joiner = new Joiner();
    for(int i=0; i<srt->getRSize();i++){
        
        sWindow[i] = new Standardtuple[srt->getSubWindowSize()]();
        
        for(int j=0; j<srt->getSubWindowSize();j++){
            sWindow[i][j] = {0};
        }
    }
    count = 0;
    ptrCount= &count;
    
}

ManagerS::ManagerS(int windowSize, int rSize, int cSize, int minRange, int maxRange){
    srt= SwRangeTable::Singleton(windowSize,rSize,cSize,minRange,maxRange);
    sWindow = new Standardtuple*[srt->getRSize()];
    extendedSWindow = new Standardtuple*[srt->getRSize()];
    entrySequence = new std::queue<int>[srt->getSubWindowSize()*srt->getRSize()];
    this->joiner = new Joiner();
    for(int i=0; i<srt->getRSize();i++){
        
        sWindow[i] = new Standardtuple[srt->getSubWindowSize()]();
        
        for(int j=0; j<srt->getSubWindowSize();j++){
            sWindow[i][j] = {0};
        }
    }
    for(int i=0; i<srt->getSubWindowSize()*srt->getRSize();i++){
        entrySequence[i] ={};
    }
    
    count = 0;
    ptrCount= &count;
    
}


void ManagerS::receiveTupleStore(Standardtuple *tuple){
    count++;
    
    ManagerAbstract::receiveTupleStore(tuple, srt, sWindow, ptrCount, entrySequence, extendedSWindow);
}

void ManagerS::receiveTupleProbe(Standardtuple *tuple){
    
    ManagerAbstract::receiveTupleProbe(tuple, srt, sWindow,extendedSWindow, joiner);
    
}


ManagerS::~ManagerS(){
    
    for(int i=0; i<srt->getRSize();i++){
        for(int j=0; j<srt->getCSize();j++){
            std::cout << srt->table[i][j] << ";";
        }
        std::cout << "\n";
    }
    
    for(int i=0; i<srt->getRSize();i++){
        
        delete[] sWindow[i];
        //delete[] extendedSWindow[i];
        sWindow[i] = nullptr;
        //extendedSWindow[i] = nullptr;
    }
    delete this->srt;
    delete[] this->sWindow;
    delete[] this->extendedSWindow;
    delete this->joiner;
    delete[] this->entrySequence;

    
    this->srt = nullptr;
    this->sWindow = nullptr;
    this->extendedSWindow = nullptr;
    this->joiner = nullptr;
    this->entrySequence =nullptr;
}