//
//  dispatcher.cpp
//  streamjoins
//
//  Created by Hassan Najeeb on 04/06/16.
//  Copyright © 2016 Hassan Najeeb. All rights reserved.
//

#include "dispatcher.hpp"


    
Dispatcher::Dispatcher(){
    this->smanager = new ManagerS();
    this->rmanager = new ManagerR();
        
    }
Dispatcher::~Dispatcher(){
    delete this->rmanager;
    delete this->smanager;
    this->rmanager = nullptr;
    this->smanager = nullptr;
    //delete this;
}

Dispatcher::Dispatcher(int windowSize, int rSize, int cSize, int minRange, int maxRange){
    this->smanager = new ManagerS(windowSize,rSize,cSize,minRange,maxRange);
    this->rmanager = new ManagerR(windowSize,rSize,cSize,minRange,maxRange);
    
}
void Dispatcher::receiveTuple(Standardtuple *tuple){
    this->key = tuple->key;
    this->window = tuple->belongsTo;
    Dispatcher::sendStore(tuple);////May
    Dispatcher::sendProbe(tuple);////   run in parallel
    
}



void Dispatcher::sendStore(Standardtuple* tuple){
    if(window == 'R'){
        rmanager->receiveTupleStore(tuple);
    }
    else if (window == 'S'){
        smanager->receiveTupleStore(tuple);
    }
}

void Dispatcher::sendProbe(Standardtuple *tuple){
    if(window == 'R'){
        smanager->receiveTupleProbe(tuple);
    }
    else if (window == 'S'){
        rmanager->receiveTupleProbe(tuple);
    }
}