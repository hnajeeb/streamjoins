//
//  PartitionFinder.cpp
//  streamjoins
//
//  Created by Hassan Najeeb on 18/07/16.
//  Copyright © 2016 Hassan Najeeb. All rights reserved.
//

#include "PartitionFinder.hpp"

int PartitionFinder::getPartition(int high, int key, RangeTable* rt){
    
    int low = 0;
    high = high-1;
    int partition = (low+high)/2;
    
    while(low <= high && !(rt->table[partition][0] <= key && key<=rt->table[partition][1])){
        
        if(key < rt->table[partition][0]){
            high = partition-1;
        }
        else if (key > rt->table[partition][1]){
            low = partition+1;
        }
        partition = (low+high)/2;
    }
    
 
    return partition;
}