//
//  TupleGenerator.hpp
//  streamjoins
//
//  Created by Hassan Najeeb on 27/07/16.
//  Copyright © 2016 Hassan Najeeb. All rights reserved.
//

#ifndef TupleGenerator_hpp
#define TupleGenerator_hpp

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "RandomGenerator.hpp"
#include "tuple.hpp"
class TupleGenerator{
public:
    
    static void writeToFileUniform(int min = 0, int max = 1000000, int numOfTuples = 10000000);
    
    static void writeToFileNormal(int min = 0, int max = 1000000, int numOfTuples = 10000000, int stdDev = 300000);
    
    static Standardtuple* getNormalTuples(int min = 0, int max = 1000000, int numOfTuples = 10000000, int stdDev = 300000);
    
    static Standardtuple* getUniformTuples(int min = 0, int max = 1000000, int numOfTuples = 10000000);
    
};
#endif /* TupleGenerator_hpp */
