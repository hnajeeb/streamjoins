//
//  RangeTable.cpp
//  streamjoins
//
//  Created by Hassan Najeeb on 06/06/16.
//  Copyright © 2016 Hassan Najeeb. All rights reserved.
//

#include "RangeTable.hpp"

int RangeTable::rSize;
int RangeTable::cSize;
int RangeTable::minRange;
int RangeTable::maxRange;
int RangeTable::windowSize;


int partitionSize = 0;
int extraSize = 0;
int minSize = 0;
int maxSize = 0;
int threshold = 0;

int RangeTable::getRSize(){
    return RangeTable::rSize;
}
int RangeTable::getCSize(){
    return RangeTable::cSize;
}
int RangeTable::getThreshold(){
    return threshold;
}
int RangeTable::getSubWindowSize(){
    return (RangeTable::windowSize/RangeTable::rSize)+(10*(RangeTable::windowSize/RangeTable::rSize));
}
int RangeTable::getWindowSize(){
    return windowSize;
}

int RangeTable::getAdjustedPartition(){
    return adjustedPartition;
}
int RangeTable::getAdjustedPartitionNeighbor(){
    return adjustedPartitionNeighbor;
}
void RangeTable::setAdjustedPartition(int adjustedPartition){
    RangeTable::adjustedPartition = adjustedPartition;
}
void RangeTable::setAdjustedPartitionNeighbor(int adjustedPartitionNeighbor){
    RangeTable::adjustedPartitionNeighbor = adjustedPartitionNeighbor;
}


RangeTable::RangeTable(int windowSize, int rSize, int cSize, int minRange, int maxRange){
    
    
    RangeTable::windowSize = windowSize; //total sliding window size
    
    RangeTable::rSize = rSize; //number of partitions
    
    RangeTable::cSize = cSize; //column of range table. should always be 9 except when you want to add/del column
    
    RangeTable::minRange = minRange; // min range for tuple keys
    
    RangeTable::maxRange = maxRange; //max range for tuple keys
    
    table = new int*[rSize];
    
    for (int i = 0; i<rSize ; i++) {
        
        table[i] = new int[cSize];
        for(int j = 0; j<cSize; j++){
            table[i][j] = -1;
        }
    }
    
    extraSize = (minRange+maxRange)%rSize;
    minSize = minRange;
    maxSize = (minRange+maxRange)/rSize;
    threshold = (windowSize/rSize)+(0.20*(windowSize/rSize));
    
    for(int i=0; i< rSize; i++){
        
        table[i][0]= minSize;
        table[i][1] = maxSize;
        table[i][2] = 0; //head
        table[i][3] = 0; //tail
        table[i][8] = threshold;
        table[i][9] = 0; //count
        table[i][11] = 0; //ehead
        table[i][12] = 0; //etail
        minSize = maxSize +1;
        maxSize = minSize + ((minRange+maxRange)/rSize)+ -1;
        
    }
    //adding the modulus value to cover the complete range
    if (extraSize!=0) {
        table[rSize-1][1] = table[rSize-1][1] + extraSize;
    }
    
}

RangeTable::~RangeTable(){
//    for(int i=0; i< rSize; i++){
//        delete[] this->table[i];
//        this->table[i] = nullptr;
//    }
    delete[] this->table;
    this->table = nullptr;
//    delete this;
}