//
//  RwRangeTable.cpp
//  streamjoins
//
//  Created by Hassan Najeeb on 07/06/16.
//  Copyright © 2016 Hassan Najeeb. All rights reserved.
//

#include "RwRangeTable.hpp"

RwRangeTable* RwRangeTable::rt;

RwRangeTable* RwRangeTable::Singleton(int windowSize, int rSize, int cSize, int minRange, int maxRange){
    
    
    
    if(RwRangeTable::rt == NULL)
    {
        //rt = new SwRangeTable(rSize,cSize, minRange, maxRange);
        //RwRangeTable::rt  = new RwRangeTable();
        RwRangeTable::rt = new RwRangeTable(windowSize, rSize,cSize, minRange, maxRange);
    }
    
    return RwRangeTable::rt ;
    
    
}

RwRangeTable::~RwRangeTable(){

}