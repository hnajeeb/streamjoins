//
//  SwRangeTable.hpp
//  streamjoins
//
//  Created by Hassan Najeeb on 07/06/16.
//  Copyright © 2016 Hassan Najeeb. All rights reserved.
//

#ifndef SwRangeTable_hpp
#define SwRangeTable_hpp
#include "RangeTable.hpp"
#include <stdio.h>

class SwRangeTable: public RangeTable{
    
private:
    SwRangeTable(int windowSize, int rSize, int cSize, int minRange, int maxRange):RangeTable(windowSize, rSize, cSize, minRange, maxRange)
    //SwRangeTable():RangeTable()
    {
        
    }
    
static SwRangeTable* rt;
    
    
    //static SWindow sw;
    
public:
    //static SwRangeTable* Singleton(int rSize = 4, int cSize = 9, int minRange = 0, int maxRange = 32767){
    static SwRangeTable* Singleton(int windowSize=10000, int rSize=100, int cSize=16, int minRange=0, int maxRange=32767);
    ~SwRangeTable();
};
#endif /* SwRangeTable_hpp */
