//
//  RandomGenerator.hpp
//  streamjoins
//
//  Created by Hassan Najeeb on 25/06/16.
//  Copyright © 2016 Hassan Najeeb. All rights reserved.
//

#ifndef RandomGenerator_hpp
#define RandomGenerator_hpp

#include <stdio.h>
#include <random>
#include <iostream>

class RandomGenerator{
    
public:
    
    int* uniformDistRandom(int from, int to, int length);
    
    char* belongsToDistRandom(int from, int to, int length);
    
    int* normalDistRandom(int from, int to, int stdDev, int length);
    
    
};

#endif /* RandomGenerator_hpp */
