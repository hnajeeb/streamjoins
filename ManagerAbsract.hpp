//
//  ManagerAbsract.hpp
//  streamjoins
//
//  Created by Hassan Najeeb on 06/06/16.
//  Copyright © 2016 Hassan Najeeb. All rights reserved.
//

#ifndef ManagerAbsract_hpp
#define ManagerAbsract_hpp
#include "tuple.hpp"
#include <stdio.h>
#include "RangeTable.hpp"
#include "tuple.hpp"
#include <queue>
#include "Joiner.hpp"
#include "PartitionFinder.hpp"
#include "commons.hpp"

class ManagerAbstract{
    bool boundaryAdjusted = false;
    //int multipleOf = 4;

public:
    ManagerAbstract();
    ~ManagerAbstract();
    void receiveTupleStore(Standardtuple *tuple, RangeTable* rt, Standardtuple** w, int *count, std::queue<int> *entrySequence, Standardtuple** extendedWindow );
    
    void receiveTupleProbe(Standardtuple *tuple, RangeTable* rt, Standardtuple** w, Standardtuple** extendedWindow, Joiner * joiner );
    
    void expirator(std::queue<int> *entrySequence, RangeTable* rt, Standardtuple** w, int *count, Standardtuple** extendedWindow);
    
    void boundaryAdjuster(Standardtuple* window, RangeTable* rt, int partition);
    
    void boundaryAdjusterHalf(Standardtuple* window, RangeTable* rt, int partition);

//    std::chrono::high_resolution_clock::time_point lastBoundaryAdjusted;
private:
    void insertInHead(std::queue<int> *entrySequence,Standardtuple** w,int partition, int head, RangeTable* rt, Standardtuple* tuple);
    void insertInEHead(std::queue<int> *entrySequence,Standardtuple** extendedWindow,int partition, int ehead, RangeTable* rt, Standardtuple* tuple);
    void deleteFromTail(RangeTable* rt, int deleteFromPartition, std::queue<int> *entrySequence, int* count);
    void deleteFromETail(RangeTable* rt, int deleteFromPartition, std::queue<int> *entrySequence, int* count);
    void checkBoundaryAdjustmentCompletetion(RangeTable* rt);
    void withExtendedBAC(int partition, int neighbor, RangeTable * rt);
    void BAC(int partition, int neighbor, RangeTable * rt);
};
#endif /* ManagerAbsract_hpp */
