//
//  SwRangeTable.cpp
//  streamjoins
//
//  Created by Hassan Najeeb on 07/06/16.
//  Copyright © 2016 Hassan Najeeb. All rights reserved.
//

#include "SwRangeTable.hpp"



SwRangeTable* SwRangeTable::rt;

SwRangeTable* SwRangeTable::Singleton(int windowSize, int rSize, int cSize, int minRange, int maxRange){
    
    
    
    if(SwRangeTable::rt == NULL)
    {
        //rt = new SwRangeTable(rSize,cSize, minRange, maxRange);
        //SwRangeTable::rt  = new SwRangeTable();
        SwRangeTable::rt  = new SwRangeTable(windowSize, rSize,cSize, minRange, maxRange);
    }
    
    return SwRangeTable::rt ;
    
    
}

SwRangeTable::~SwRangeTable(){

}