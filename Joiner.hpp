//
//  Joiner.hpp
//  streamjoins
//
//  Created by Hassan Najeeb on 06/06/16.
//  Copyright © 2016 Hassan Najeeb. All rights reserved.
//

#ifndef Joiner_hpp
#define Joiner_hpp
#include "tuple.hpp"
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "RangeTable.hpp"
#include "commons.hpp"

class Joiner{
private:
    int count = 0;
    Standardtuple **results, **testResults;
    int resultCount = 0, sizeofResults = 0;

public:
    
    Joiner();
    void receiveTuple(Standardtuple *tuple, Standardtuple *window, int head, int tail, int count, Standardtuple ** extendedWindow, int exPartBit, int ehead, int etail, int elength, int partition, RangeTable *rt);
    
    void testJoin(Standardtuple *tuple, Standardtuple **probeWindow, int partitions, int partitionSize, RangeTable* rt, Standardtuple ** extendedWindow);
    ~Joiner();
};



#endif /* Joiner_hpp */
