//
//  commons.h
//  streamjoins
//
//  Created by Hassan Najeeb on 27/07/16.
//  Copyright © 2016 Hassan Najeeb. All rights reserved.
//

#ifndef commons_h
#define commons_h
//#define testBit //managerabstract
//#define inputFromConsole
//#define fileReader //main class
#define normal //main class
//#define dataGenerator
//#define printForComparison //joiner

//#define whichPartitionExtended //managerAbstract
//#define whichExtendedPartitionDeleted //managerAbstract
//#define printWholeRangeTableAfterBA //managerAbstract
//#define whichPatitionsAdjusted //managerAbstract
//#define whenBAComplete //managerAbstract
//#define printOnlyAdjustedPartitons //managerAbstract
//#define runScripts

#define boundaryAdjusterDivideby4
//#define boundaryAdjusterDivideBy2


const int multipleOf = 4; //size of extended partition
#endif /* commons_h */
