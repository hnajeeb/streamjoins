//
//  main.cpp
//  streamjoins
//
//  Created by Hassan Najeeb on 04/06/16.
//  Copyright © 2016 Hassan Najeeb. All rights reserved.
//


#include <iostream>
#include "dispatcher.hpp"
#include "tuple.hpp"
#include <string>
#include <fstream>
#include <sstream>
#include <chrono>
#include "TupleGenerator.hpp"
#include "commons.hpp"


int main(int argc, const char * argv[]) {
    
    int cSize = 16; //column of range table. should always be 16 except when you want to add/del column
    int numberOfTuples=10000, windowSize=250,rSize=5,minRange=0,maxRange=100,stdDev=10;
    
    /*************************************Data Input Arguments Section Start***********************************************/
#ifndef inputFromConsole
#ifdef normal
    stdDev = atoi(argv[5]);
    numberOfTuples = atoi(argv[6]);
#endif
#ifndef normal
    numberOfTuples = atoi(argv[5]);
#endif
    windowSize = atoi(argv[1]);
    rSize = atoi(argv[2]);
    minRange = atoi(argv[3]);
    maxRange = atoi(argv[4]);
#endif
    /*************************************Data Input Arguments Section End***********************************************/
    
    
    
    
    
    
    /***************************Data Input Console Section Start*************************************************/
    
#ifdef inputFromConsole
    
#ifdef normal
    std::cout << "Enter size for (sliding window, number of partitions, min range, max range, std dev, number of tuples) separated by spaces" << std::endl;
    scanf("%i %i %i %i %i %i", &windowSize,&rSize,&minRange,&maxRange,&stdDev,&numberOfTuples);
    std::cout<<std::endl;
#endif //ifdef normal
    
#ifndef normal
    std::cout << "Enter size for (sliding window, number of partitions, min range, max range, number of tuples) separated by spaces" << std::endl;
    scanf("%i %i %i %i %i", &windowSize,&rSize,&minRange,&maxRange,&numberOfTuples);
    std::cout<<std::endl;
#endif//ifndef normal
    
#endif//ifdef inputFromConsole
    /*****************************Data Input Console Section End***************************************************/
    
    
    
    
    
    /*****************************Common Section I Start***************************************************/
    std::chrono::high_resolution_clock::time_point t1;
    std::chrono::high_resolution_clock::time_point t2;
    Dispatcher *obj = new Dispatcher(windowSize,rSize,cSize,minRange,maxRange);
    //TupleGenerator tupleGenerator;
    Standardtuple* tuples = new Standardtuple[numberOfTuples];
    /*****************************Common Section I End***************************************************/
    
    
    
    
    
    /*##########################FileReader Section Start######################################*/
#ifdef fileReader
    
    
#ifdef dataGenerator
#ifdef normal
    TupleGenerator::writeToFileNormal(minRange,maxRange,numberOfTuples,stdDev);
#endif //ifdef normal
    
#ifndef normal
    TupleGenerator::writeToFileUniform(minRange,maxRange,numberOfTuples);
#endif // ifndef normal(for Uniform)
#endif //data generation
    
    std::ifstream readTuples("/Users/hassan/Documents/Testing/Testing/data.txt");
    std::string inLine;
    int count=0;
    while(std::getline(readTuples,inLine)){
        std::istringstream iss(inLine);
        //Standardtuple *tuple = new Standardtuple();
        int key;
        char win;
        
        iss>>key>>win;
        tuples[count].key = key;
        tuples[count].belongsTo = win;
        count++;
    }
    
    
#endif
    /*##########################FileReader Section End######################################*/
    
    
    
    
    
#ifndef fileReader
    
#ifdef normal
    tuples = TupleGenerator::getNormalTuples(minRange,maxRange,numberOfTuples,stdDev);
#endif//ifdef normal
    
#ifndef normal
    tuples = TupleGenerator::getUniformTuples(minRange,maxRange,numberOfTuples);
#endif //ifndef normal
    
#endif
    
    
    
    /*****************************Common Section II Start***************************************************/
    
    t1 = std::chrono::high_resolution_clock::now();
    std::chrono::high_resolution_clock::time_point throughputStart = t1;
    std::chrono::high_resolution_clock::time_point throughputEnd;
    int processedTuples=0;
    
    for(int i = 0; i < numberOfTuples; i++){
        Standardtuple *tuple = new Standardtuple();
        
        tuple->key = tuples[i].key;
        tuple->belongsTo = tuples[i].belongsTo;
        
        obj->receiveTuple(tuple);

        auto timeInSeconds = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now()- throughputStart).count();
        
        if( timeInSeconds == 1){
            std::cout << i-processedTuples << "\n";
            processedTuples=i;
            throughputStart = std::chrono::high_resolution_clock::now();
        }
        
//        if(i%100000 == 0){
//            
//            std::chrono::high_resolution_clock::time_point throughputEnd = std::chrono::high_resolution_clock::now();
//            auto diff = std::chrono::duration_cast<std::chrono::microseconds>( throughputEnd - throughputStart ).count();
//            double durationInSeconds = diff/1000000.00;
//            std::cout << durationInSeconds << "\n";
//            throughputStart = std::chrono::high_resolution_clock::now();
//        }
    }
    t2 = std::chrono::high_resolution_clock::now();
    delete obj;
    delete[] tuples;
    obj = nullptr;
    tuples = nullptr;

    auto duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
    double durationInSeconds = duration/1000000.00;
    std::cout << "\n" <<durationInSeconds << "\n";
    std::cout << "ends\n";
    
    duration = 0;
    windowSize = 0;
    rSize = 0;
    minRange = 0;
    maxRange = 0;
    stdDev = 0;
    numberOfTuples = 0;
    
    return 0;
    /*****************************Common Section II End***************************************************/
    
}
