//
//  Joiner.cpp
//  streamjoins
//
//  Created by Hassan Najeeb on 06/06/16.
//  Copyright © 2016 Hassan Najeeb. All rights reserved.
//

#include "Joiner.hpp"

Joiner::Joiner(){
    sizeofResults = 100000;
    results = new Standardtuple*[sizeofResults]();
    testResults = new Standardtuple*[sizeofResults]();
    //int *debugging = new int[100];
    for(int i=0;i<sizeofResults;i++){
        results[i] = new Standardtuple[2];
        results[i][0] = results[i][1] = {};
        testResults[i] = new Standardtuple[2];  // testing
        testResults[i][0] = testResults[i][1] = {}; //testing
    }
}

Joiner::~Joiner(){
//    delete[] results;
//    delete [] testResults;
//    delete this;
}
//int rcount = 0;

void Joiner::receiveTuple(Standardtuple *tuple, Standardtuple *window, int head, int tail, int lenOfPartition, Standardtuple ** extendedWindow, int exPartBit, int ehead, int etail, int elength, int partition, RangeTable *rt){
    resultCount = 0;
    if(tail == -1){
        tail = 0;
    }
    if(head>tail){
        tail++;
        for(int i = tail; i <= head; i++){
            if(tuple->key == window[i].key){
                results[resultCount][0] = *tuple;
                results[resultCount][1] = window[i];
                resultCount++;
            }
        }
    }
    else if(head<tail){
        tail++;
        for(int i = tail; i <= lenOfPartition-1; i++){
            if(tuple->key == window[i].key ){
                results[resultCount][0] = *tuple;
                results[resultCount][1] = window[i];
                resultCount++;
            }
        }
        for(int i = 0; i <= head; i++){
            if(tuple->key == window[i].key ){
                results[resultCount][0] = *tuple;
                results[resultCount][1] = window[i];
                resultCount++;
            }
        }
    }
    
    //if there is an extended window
    if(exPartBit == 1){
        if(etail == -1){
            etail = 0;
        }
        
        int elenOfPartition = elength;
        if(ehead>etail){
            etail++;
            for(int j = etail; j <= ehead; j++){
                if(tuple->key == extendedWindow[partition][j].key ){
                    results[resultCount][0] = *tuple;
                    results[resultCount][1] = extendedWindow[partition][j];
                    resultCount++;
                }
            }
        }
        
        else if(ehead<etail){
            etail++;
            for(int j = etail; j <= elenOfPartition-1; j++){
                if(tuple->key == extendedWindow[partition][j].key ){
                    results[resultCount][0] = *tuple;
                    results[resultCount][1] = extendedWindow[partition][j];
                    resultCount++;
                }
            }
            for(int j = 0; j <= ehead; j++){
                if(tuple->key == extendedWindow[partition][j].key ){
                    results[resultCount][0] = *tuple;
                    results[resultCount][1] = extendedWindow[partition][j];
                    resultCount++;
                }
            }
        }
    }
#ifdef printForComparison
    std::ofstream tuples("/Users/hassan/Documents/Testing/Testing/results.txt",std::ofstream::app);
    for(int i = 0; i<= resultCount-1; i++){
        tuples << results[i][0].key << results[i][0].belongsTo;
        tuples << results[i][1].key << results[i][1].belongsTo;
        tuples << "\n";
    }
#endif
    
}

//testing
int printCount = 0;

void Joiner::testJoin(Standardtuple *tuple, Standardtuple **probeWindow, int partitions, int partitionSize, RangeTable* rt, Standardtuple ** extendedWindow){ //testing
    
    resultCount = 0;
    for(int i = 0; i < rt->getRSize(); i++){
        
        int head = rt->table[i][2];
        int tail = rt->table[i][3];
        int lenOfPartition = rt->getSubWindowSize();
        
        if(tail == -1){
            tail = 0;
        }
        
        if(head>tail){
            tail++;
            for(int j = tail; j <= head; j++){
                if(tuple->key == probeWindow[i][j].key ){
                    
                    if(tuple->belongsTo == probeWindow[i][j].belongsTo){
                        std::cout << "\nHere you go" << tuple->key;
                        for(int k=0;k<rt->getRSize();k++){
                            for(int l=0;l<rt->getCSize();l++){
                                std::cout << rt->table[k][l] << ",";
                            }
                            std::cout << "\n";
                        }
                        std::cout << "\n";
                    }
                    testResults[resultCount][0] = *tuple;
                    testResults[resultCount][1] = probeWindow[i][j];
                    resultCount++;
                    printCount++;
                }
            }
        }
        
        else if(head<tail){
            tail++;
            for(int j = tail; j <= lenOfPartition-1; j++){
                if(tuple->key == probeWindow[i][j].key ){
                    if(tuple->belongsTo == probeWindow[i][j].belongsTo){
                        std::cout << "\nHere you go" << tuple->key;
                        for(int k=0;k<rt->getRSize();k++){
                            for(int l=0;l<rt->getCSize();l++){
                                std::cout << rt->table[k][l] << ",";
                            }
                            std::cout << "\n";
                        }
                        std::cout << "\n";
                    }
                    testResults[resultCount][0] = *tuple;
                    testResults[resultCount][1] = probeWindow[i][j];
                    resultCount++;
                    printCount++;
                }
            }
            for(int j = 0; j <= head; j++){
                if(tuple->key == probeWindow[i][j].key ){
                    if(tuple->belongsTo == probeWindow[i][j].belongsTo){
                        std::cout << "\nHere you go" << tuple->key;
                        for(int k=0;k<rt->getRSize();k++){
                            for(int l=0;l<rt->getCSize();l++){
                                std::cout << rt->table[k][l] << ",";
                            }
                            std::cout << "\n";
                        }
                        std::cout << "\n";
                    }
                    testResults[resultCount][0] = *tuple;
                    testResults[resultCount][1] = probeWindow[i][j];
                    resultCount++;
                    printCount++;
                }
            }
        }
        //if there is an extended window
        if(rt->table[i][10] == 1){
            int ehead = rt->table[i][11];
            int etail = rt->table[i][12];
            if(etail == -1){
                etail = 0;
            }
            
            int elenOfPartition = rt->getThreshold()*multipleOf;
            if(ehead>etail){
                etail++;
                for(int j = etail; j <= ehead; j++){
                    if(tuple->key == extendedWindow[i][j].key ){
                        if(tuple->belongsTo == extendedWindow[i][j].belongsTo){
                            std::cout << "\nHere you go" << tuple->key;
                            for(int k=0;k<rt->getRSize();k++){
                                for(int l=0;l<rt->getCSize();l++){
                                    std::cout << rt->table[k][l] << ",";
                                }
                                std::cout << "\n";
                            }
                            std::cout << "\n";
                        }
                        testResults[resultCount][0] = *tuple;
                        testResults[resultCount][1] = extendedWindow[i][j];
                        resultCount++;
                        printCount++;
                    }
                }
            }
            
            else if(ehead<etail  && ehead!=-1){
                etail++;
                for(int j = etail; j <= elenOfPartition-1; j++){
                    if(tuple->key == extendedWindow[i][j].key ){
                        if(tuple->belongsTo == extendedWindow[i][j].belongsTo){
                            std::cout << "\nHere you go" << tuple->key;
                            for(int k=0;k<rt->getRSize();k++){
                                for(int l=0;l<rt->getCSize();l++){
                                    std::cout << rt->table[k][l] << ",";
                                }
                                std::cout << "\n";
                            }
                            std::cout << "\n";
                        }
                        testResults[resultCount][0] = *tuple;
                        testResults[resultCount][1] = extendedWindow[i][j];
                        resultCount++;
                        printCount++;
                    }
                }
                for(int j = 0; j <= ehead; j++){
                    if(tuple->key == extendedWindow[i][j].key ){
                        if(tuple->belongsTo == extendedWindow[i][j].belongsTo){
                            std::cout << "\nHere you go" << tuple->key;
                            for(int k=0;k<rt->getRSize();k++){
                                for(int l=0;l<rt->getCSize();l++){
                                    std::cout << rt->table[k][l] << ",";
                                }
                                std::cout << "\n";
                            }
                            std::cout << "\n";
                        }
                        testResults[resultCount][0] = *tuple;
                        testResults[resultCount][1] = extendedWindow[i][j];
                        resultCount++;
                        printCount++;
                    }
                }
            }
        }
    }
    

#ifdef printForComparison
    std::ofstream tuples("/Users/hassan/Documents/Testing/Testing/testresults.txt",std::ofstream::app);
    for(int i = 0; i<= resultCount-1; i++){
        tuples << testResults[i][0].key << testResults[i][0].belongsTo;
        tuples << testResults[i][1].key << testResults[i][1].belongsTo;
        tuples << "\n";
        count++;
    }
#endif
    
}