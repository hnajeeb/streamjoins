//
//  PartitionFinder.hpp
//  streamjoins
//
//  Created by Hassan Najeeb on 18/07/16.
//  Copyright © 2016 Hassan Najeeb. All rights reserved.
//

#ifndef PartitionFinder_hpp
#define PartitionFinder_hpp

#include <stdio.h>
#include "RangeTable.hpp"

class PartitionFinder{
    
public:
    static int getPartition(int high, int key, RangeTable* rt);
};

#endif /* PartitionFinder_hpp */
