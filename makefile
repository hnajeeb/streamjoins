SOURCE=main.cpp dispatcher.cpp TupleGenerator.cpp PartitionFinder.cpp RandomGenerator.cpp SwRangeTable.cpp RwRangeTable.cpp RangeTable.cpp Joiner.cpp ManagerS.cpp ManagerR.cpp ManagerAbsract.cpp tuple.cpp
MYPROGRAM=streamjoins

CC=clang++
FLAGS=-std=c++11 -stdlib=libc++



all: $(MYPROGRAM)



$(MYPROGRAM): $(SOURCE)

	$(CC) $(FLAGS)  $(SOURCE) -o $(MYPROGRAM)

clean:

	rm -f $(MYPROGRAM)
