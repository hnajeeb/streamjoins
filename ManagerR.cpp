//
//  ManagerR.cpp
//  streamjoins
//
//  Created by Hassan Najeeb on 08/06/16.
//  Copyright © 2016 Hassan Najeeb. All rights reserved.
//

#include "ManagerR.hpp"

ManagerR::ManagerR(){
    rrt= RwRangeTable::Singleton();
    rWindow = new Standardtuple*[rrt->getRSize()];
    extendedRWindow = new Standardtuple*[rrt->getRSize()];
    entrySequence = new std::queue<int>[rrt->getSubWindowSize()*rrt->getRSize()];
    this->joiner = new Joiner();
    
    for(int i=0; i<rrt->getRSize();i++){
        
        rWindow[i] = new Standardtuple[rrt->getSubWindowSize()]();
        for(int j=0; j<rrt->getSubWindowSize();j++){
            rWindow[i][j] = {0};
        }
    }
    count = 0;
    ptrCount= &count;
}

ManagerR::ManagerR(int windowSize, int rSize, int cSize, int minRange, int maxRange){
    rrt= RwRangeTable::Singleton(windowSize,rSize,cSize,minRange,maxRange);
    rWindow = new Standardtuple*[rrt->getRSize()];
    extendedRWindow = new Standardtuple*[rrt->getRSize()];
    entrySequence = new std::queue<int>[rrt->getSubWindowSize()*rrt->getRSize()]{};
    this->joiner = new Joiner();
    
    for(int i=0; i<rrt->getRSize();i++){
        
        rWindow[i] = new Standardtuple[rrt->getSubWindowSize()]();
        for(int j=0; j<rrt->getSubWindowSize();j++){
            rWindow[i][j] = {0};
        }
    }
    for(int i=0; i<rrt->getSubWindowSize()*rrt->getRSize();i++){
        entrySequence[i] ={};
    }
    
    count = 0;
    ptrCount= &count;
}

void ManagerR::receiveTupleStore(Standardtuple *tuple){
    count++;
    if(tuple->belongsTo == 'S'){
        std::cout << "let me know";
    }
    ManagerAbstract::receiveTupleStore(tuple, rrt, rWindow, ptrCount, entrySequence, extendedRWindow);
}

void ManagerR::receiveTupleProbe(Standardtuple *tuple){
    
    ManagerAbstract::receiveTupleProbe(tuple, rrt, rWindow, extendedRWindow, joiner);
    
}


ManagerR::~ManagerR(){
    
    for(int i=0; i<rrt->getRSize();i++){
        for(int j=0; j<rrt->getCSize();j++){
            std::cout << rrt->table[i][j] << ";";
        }
        std::cout << "\n";
    }
    for(int i=0; i<rrt->getRSize();i++){
        
        delete[] rWindow[i];
        delete[] extendedRWindow[i];
        rWindow[i] = nullptr;
        extendedRWindow[i] = nullptr;
    }
    
    delete this->rrt;
    delete[] this->rWindow;
    delete[] this->extendedRWindow;
    delete this->joiner;
    delete[] this->entrySequence;
    
    this->rrt = nullptr;
    this->rWindow = nullptr;
    this->extendedRWindow = nullptr;
    this->joiner = nullptr;
    this->entrySequence = nullptr;
    
}