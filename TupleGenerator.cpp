//
//  TupleGenerator.cpp
//  streamjoins
//
//  Created by Hassan Najeeb on 27/07/16.
//  Copyright © 2016 Hassan Najeeb. All rights reserved.
//

#include "TupleGenerator.hpp"

RandomGenerator randList;

void TupleGenerator::writeToFileUniform(int min, int max, int numOfTuples){
    
    int  *testTuplesKeys = nullptr;
    char *tupleBelonging;
    
    
    //    int min = 0;
    //    int max = 1000000;
    //    int numOfTuples=10000000;
    
    testTuplesKeys = randList.uniformDistRandom(min, max, numOfTuples);
    
    tupleBelonging = randList.belongsToDistRandom(0, 1, numOfTuples);
    
    std::ofstream tuples("/Users/hassan/Documents/Testing/Testing/data.txt");
    for(int i = 0; i< numOfTuples; i++){
        tuples << testTuplesKeys[i];
        tuples << tupleBelonging[i];
        tuples << "\n";
    }
}

void TupleGenerator::writeToFileNormal(int min, int max, int numOfTuples, int stdDev){
    
    
    int  *testTuplesKeys = nullptr;
    char *tupleBelonging;
    
    
    //    int min = 0;
    //    int max = 1000000;
    //    int numOfTuples=10000000;
    //    int stdDev = 300000;
    
    
    testTuplesKeys = randList.normalDistRandom(min, max, stdDev, numOfTuples);
    
    tupleBelonging = randList.belongsToDistRandom(0, 1, numOfTuples);
    
    std::ofstream tuples("/Users/hassan/Documents/Testing/Testing/data.txt");
    for(int i = 0; i< numOfTuples; i++){
        tuples << testTuplesKeys[i];
        tuples << tupleBelonging[i];
        tuples << "\n";
    }
}

//Standardtuple* TupleGenerator::getNormalTuples(int min, int max, int numOfTuples, int stdDev){
//    
//    Standardtuple* tuples = new Standardtuple[numOfTuples];
//    int  *testTuplesKeys;// = nullptr;
//    char *tupleBelonging;// = nullptr;
//    
//    testTuplesKeys = randList.normalDistRandom(min, max, stdDev, numOfTuples);
//    
//    tupleBelonging = randList.belongsToDistRandom(0, 1, numOfTuples);
//    
//    for(int i = 0; i < numOfTuples-1; i++){
//        tuples[i].key = testTuplesKeys[i];
//        tuples[i].belongsTo = tupleBelonging[i];
//    }
//    
//    delete[] testTuplesKeys;
//    delete [] tupleBelonging;
//    
//    return tuples;
//    
//}

Standardtuple* TupleGenerator::getNormalTuples(int min, int max, int numOfTuples, int stdDev){
    
    Standardtuple* tuples = new Standardtuple[numOfTuples];
    int  *testTuplesKeys;// = nullptr;
    char *tupleBelonging;// = nullptr;
    
    testTuplesKeys = randList.uniformDistRandom(min, max, 5000000);
    
    tupleBelonging = randList.belongsToDistRandom(0, 1, 5000000);
    
    for(int i = 0; i < 5000000; i++){
        tuples[i].key = testTuplesKeys[i];
        tuples[i].belongsTo = tupleBelonging[i];
    }
    testTuplesKeys = randList.normalDistRandom(min, max, stdDev, 195000000);
    
    tupleBelonging = randList.belongsToDistRandom(0, 1, 195000000);
    
    for(int i = 0; i < 195000000; i++){
        tuples[i+5000000].key = testTuplesKeys[i];
        tuples[i+5000000].belongsTo = tupleBelonging[i];
    }
    
    delete[] testTuplesKeys;
    delete [] tupleBelonging;
    
    return tuples;
    
}


Standardtuple* TupleGenerator::getUniformTuples(int min, int max, int numOfTuples){
    
    Standardtuple* tuples = new Standardtuple[numOfTuples];
    int  *testTuplesKeys = nullptr;
    char *tupleBelonging;
    
    testTuplesKeys = randList.uniformDistRandom(min, max, numOfTuples);
    
    tupleBelonging = randList.belongsToDistRandom(0, 1, numOfTuples);
    
    for(int i = 0; i < numOfTuples; i++){
        tuples[i].key = testTuplesKeys[i];
        tuples[i].belongsTo = tupleBelonging[i];
    }
    return tuples;
}
