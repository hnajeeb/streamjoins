//
//  RandomGenerator.cpp
//  streamjoins
//
//  Created by Hassan Najeeb on 25/06/16.
//  Copyright © 2016 Hassan Najeeb. All rights reserved.
//

#include "RandomGenerator.hpp"

int* RandomGenerator::uniformDistRandom(int from, int to, int length){
    int *randomInt = new int[length];
    
    std::random_device rand_dev;
    std::mt19937 generator(rand_dev());
    std::uniform_int_distribution<int>  distr(from, to);
    
    for(int i =0; i<length; i++){
        randomInt[i] = distr(generator);
    }
    
    return randomInt;
}
char* RandomGenerator::belongsToDistRandom(int from, int to, int length){
    char *randomWin = new char[length];
    
    std::random_device rand_dev;
    std::mt19937 generator(rand_dev());
    std::uniform_int_distribution<int>  distr(from, to);
//    int num;
    for(int i =0; i<length; i++){

        if(i%2 == 0){
            randomWin[i] = 'S';
        }
        else{
            randomWin[i] = 'R';
        }
//        num = distr(generator);
//        if (num == 0) {
//            randomWin[i] = 'S';
//        }
//        else{
//            randomWin[i] = 'R';
//        }
    }
    
    return randomWin;
}

int* RandomGenerator::normalDistRandom(int from, int to, int stdDev, int length){
    
    int *randomInt = new int[length];
    
    std::random_device rand_dev;
    std::mt19937 generator(rand_dev());
    int mean = (from+to)/2;
    std::normal_distribution<> distr(mean, stdDev);
    
    int count = 0;
    
    while(count != length){
        int i = std::round((distr(generator)));
        if(from< i && i<=to){
            randomInt[count] = i;
            count++;
        }
    }
    
    return randomInt;
}
