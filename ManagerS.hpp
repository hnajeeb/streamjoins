//
//  ManagerS.hpp
//  streamjoins
//
//  Created by Hassan Najeeb on 08/06/16.
//  Copyright © 2016 Hassan Najeeb. All rights reserved.
//

#ifndef ManagerS_hpp
#define ManagerS_hpp
#include "ManagerAbsract.hpp"
#include "SwRangeTable.hpp"
#include "tuple.hpp"
#include <stdio.h>
#include <queue>
class ManagerS: public ManagerAbstract{
    
private:
    int count;
    int *ptrCount;
    std::queue<int> *entrySequence;
    
public:
    SwRangeTable* srt;
    Standardtuple** sWindow;
    Standardtuple** extendedSWindow;
    Joiner *joiner;
    
    ManagerS();
    ManagerS(int windowSize, int rSize, int cSize, int minRange, int maxRange);
    void receiveTupleStore(Standardtuple *tuple);
    
    void receiveTupleProbe(Standardtuple *tuple);
    ~ManagerS();
};
#endif /* ManagerS_hpp */
