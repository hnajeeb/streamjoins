//
//  RangeTable.hpp
//  streamjoins
//
//  Created by Hassan Najeeb on 06/06/16.
//  Copyright © 2016 Hassan Najeeb. All rights reserved.
//

#ifndef RangeTable_hpp
#define RangeTable_hpp
#include <string>
#include <stdio.h>
#include <iostream>

class RangeTable{
public:
    
    int **table;
    
    int getRSize();
    int getCSize();
    int getThreshold();
    int getSubWindowSize();
    int getWindowSize();
    int getAdjustedPartition();
    int getAdjustedPartitionNeighbor();
    void setAdjustedPartition(int adjustedPartition);
    void setAdjustedPartitionNeighbor(int adjustedPartitionNeighbor);

    int adjustedPartition;
    int adjustedPartitionNeighbor;
    ~RangeTable();
/*
    0 min
    1 max
    2 head
    3 tail
    4 oldMin
    5 oldMax
    6 oldHead
    7 oldTail
    8 threshold
    9 count
    10 eMemFlag
    11 eHead
    12 eTail
    13 jumpPointer
    14 oldEHead
    15 oldETail
*/
    
    
public:
    
    static int rSize;
    static int cSize;
    static int minRange;
    static int maxRange;
    static int windowSize;
    

    
protected:
    
    RangeTable(int windowSize, int rSize, int cSize, int minRange, int maxRange);
};
#endif /* RangeTable_hpp */
