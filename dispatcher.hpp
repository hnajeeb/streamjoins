//
//  dispatcher.hpp
//  streamjoins
//
//  Created by Hassan Najeeb on 04/06/16.
//  Copyright © 2016 Hassan Najeeb. All rights reserved.
//

#ifndef dispatcher_hpp
#define dispatcher_hpp
#include <string>
#include "tuple.hpp"
#include "ManagerS.hpp"
#include "ManagerR.hpp"
#include <stdio.h>

class Dispatcher{
public:
    int key;
    char window;
    Dispatcher(int windowSize, int rSize, int cSize, int minRange, int maxRange);
    Dispatcher();
    ManagerS *smanager;
    ManagerR *rmanager;
    void receiveTuple(Standardtuple *tuple);
    ~Dispatcher();

private:
    void sendStore(Standardtuple *tuple);
    void sendProbe(Standardtuple *tuple);
};

#endif /* dispatcher_hpp */
